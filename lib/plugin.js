import Vue from 'vue';
import GMap from <%= serialize(options.components.GMap) %>;
import GMapMarker from <%= serialize(options.components.GMapMarker) %>;
import GMapCircle from <%= serialize(options.components.GMapCircle) %>;
import GMapInfoWindow from <%= serialize(options.components.GMapInfoWindow) %>;

export default(context, inject) =>{
  const runtimeConfig = context.$config && context.$config.gmaps || {}

  // key
  const key = runtimeConfig.key || process.env._GMAPS_KEY_ || (<%= serialize(options.key) %>)

  // libraries
  const libraries = runtimeConfig.libraries || process.env._GMAPS_LIBRARIES_ || (<%= serialize(options.libraries) %>) || ['drawing']
  
  Vue.component('GMap', GMap);
  Vue.component('GMapMarker', GMapMarker);
  Vue.component('GMapCircle', GMapCircle);
  Vue.component('GMapInfoWindow', GMapInfoWindow);
  inject('GMaps', {apiKey: key, loaded: false, libraries: libraries})
}
